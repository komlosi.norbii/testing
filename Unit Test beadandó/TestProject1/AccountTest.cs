using Homework.ThirdParty;
using Moq;
using NUnit.Framework;

namespace Homework.UnitTests
{
    public class AccountTest { 
    
        private Mock<IAction> mockAction;

        [SetUp]
        public void Setup()
        {
            mockAction = new Mock<IAction>();
        }

        [Test]
        public void TakeAction_ExecutesTests()
        {
            Account account = new Account(0);
            account.Activate();

            account.TakeAction(mockAction.Object);

            mockAction.Verify(action => action.Execute(), Times.Once);
            mockAction.VerifyNoOtherCalls();
        }

        [Test]
        public void TakeActionResult_EqualsActionExecuteResult()
        {
            Account account = new Account(0);
            account.Activate();

            mockAction.Setup(action => action.Execute())
                .Returns(false);
            Assert.That(account.TakeAction(mockAction.Object), Is.False);

            mockAction.Setup(action => action.Execute())
                .Returns(true);
            Assert.That(account.TakeAction(mockAction.Object), Is.True);
        }

        [Test]
        public void ActionWithNotActivatedUser_ThrowsTest()
        {
            Account account = new Account(0);

            Assert.Throws<InactiveUserException>(() => account.TakeAction(mockAction.Object));
            account.Activate();
            Assert.DoesNotThrow(() => account.TakeAction(mockAction.Object));
        }

        [Test]
        public void ActionSuccess_IncreasesPropertyTest()
        {
            Account account = new Account(0);
            account.Activate();

            account.TakeAction(mockAction.Object);
            Assert.That(account.ActionsSuccessfullyPerformed, Is.EqualTo(0));

            mockAction.Setup(action => action.Execute())
                .Returns(true);

            account.TakeAction(mockAction.Object);
            Assert.That(account.ActionsSuccessfullyPerformed, Is.EqualTo(1));
        }

        [Test]
        public void RegistrationTest()
        {
            Account account = new Account(0);

            Assert.That(account.IsRegistered, Is.False);

            account.Register();
            Assert.That(account.IsRegistered, Is.True);
        }

        [Test]
        public void ActivationTest()
        {
            Account account = new Account(0);

            Assert.That(account.IsConfirmed, Is.False);

            account.Activate();
            Assert.That(account.IsConfirmed, Is.True);
        }

        [TestCase(0)]
        [TestCase(10)]
        [TestCase(99)]
        [TestCase(int.MaxValue)]
        public void IdPropertyTest(int id)
        {
            Account account = new Account(id);
            Assert.That(account.Id, Is.EqualTo(id));
        }
    }
}