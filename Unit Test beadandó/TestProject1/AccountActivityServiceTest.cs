﻿using Homework.ThirdParty;
using Moq;
using NUnit.Framework;

namespace Homework.UnitTests
{
    public class AccountActivityServiceTest
    {
        private Mock<IAction> mockAction;
        private Mock<IAccountRepository> mockRepo;
        private AccountActivityService service;

        [SetUp]
        public void Setup()
        {
            mockRepo = new Mock<IAccountRepository>();
            mockAction = new Mock<IAction>();
            service = new AccountActivityService(mockRepo.Object);
        }

        [Test]
        public void NotExistingAccount_Throws()
        {
            Account nullAccount = null;
            Account account = new Account(0);
            mockRepo.Setup(repo => repo.Get(0))
                .Returns(account);
            mockRepo.Setup(repo => repo.Get(1))
                .Returns(nullAccount);

            Assert.DoesNotThrow(() => service.GetActivity(0));
            Assert.Throws<AccountNotExistsException>(() => service.GetActivity(1));
        }

        [TestCase(0, ActivityLevel.None)]
        [TestCase(1, ActivityLevel.Low)]
        [TestCase(19, ActivityLevel.Low)]
        [TestCase(20, ActivityLevel.Medium)]
        [TestCase(39, ActivityLevel.Medium)]
        [TestCase(40, ActivityLevel.High)]
        [TestCase(1000, ActivityLevel.High)]
        public void GetActivityLevel_ReturnValueTest(int actionCount, ActivityLevel expectedLevel)
        {
            Account account = new Account(0);
            account.Register();
            account.Activate();
            mockRepo.Setup(repo => repo.Get(0))
                .Returns(account);
            mockAction.Setup(action => action.Execute())
                .Returns(true);

            for (int i = 0; i < actionCount; i++)
            {
                account.TakeAction(mockAction.Object);
            }
            Assert.AreEqual(service.GetActivity(0), expectedLevel);
        }

        [Test]
        public void GetActivityLevel_BehaviourTest()
        {
            Account account = new Account(0);
            account.Register();
            account.Activate();
            mockRepo.Setup(repo => repo.Get(0))
                .Returns(account);

            service.GetActivity(0);

            mockRepo.Verify(repo => repo.Get(0), Times.Once);
            mockRepo.VerifyNoOtherCalls();
        }

        [TestCase(new[]{ 0, 10, 20, 40 }, 1, 1, 1, 1)]
        [TestCase(new[] { 0, 0, 0, 10, 15, 22, 999 }, 3, 2, 1, 1)]
        [TestCase(new[] { 10, 11, 12, 40 }, 0, 3, 0, 1)]
        [TestCase(new[] { 0, 20, 21, 22 }, 1, 0, 3, 0)]
        [TestCase(new[] { 10, 40, 41, 42 }, 0, 1, 0, 3)]
        public void GetAmountForActivityTest(int[] successfulActions, int none, int low, int medium, int high)
        {
            mockAction.Setup(action => action.Execute())
                .Returns(true);

            Account[] accounts = new Account[successfulActions.Length];
            mockRepo.Setup(repo => repo.GetAll())
                .Returns(accounts);

            for (int i = 0; i < accounts.Length; i++)
            {
                accounts[i] = new Account(i);
                accounts[i].Activate();
                mockRepo.Setup(repo => repo.Get(i))
                    .Returns(accounts[i]);
                for (int j = 0; j < successfulActions[i]; j++)
                {
                    accounts[i].TakeAction(mockAction.Object);
                }
            }

            Assert.That(service.GetAmountForActivity(ActivityLevel.None), Is.EqualTo(none));
            Assert.That(service.GetAmountForActivity(ActivityLevel.Low), Is.EqualTo(low));
            Assert.That(service.GetAmountForActivity(ActivityLevel.Medium), Is.EqualTo(medium));
            Assert.That(service.GetAmountForActivity(ActivityLevel.High), Is.EqualTo(high));
        }

        public void GetAmountForActivity_BeaviourTest()
        {
            mockAction.Setup(action => action.Execute())
                .Returns(true);

            Account[] accounts = new Account[2] { new Account(0), new Account(1) };
            accounts[0].Activate();
            accounts[1].Activate();

            mockRepo.Setup(repo => repo.GetAll())
                .Returns(accounts);
            mockRepo.Setup(repo => repo.Get(0))
                .Returns(accounts[0]);
            mockRepo.Setup(repo => repo.Get(1))
                .Returns(accounts[1]);

            service.GetAmountForActivity(ActivityLevel.None);
            mockRepo.Verify(repo => repo.GetAll(), Times.Once);
            mockRepo.Verify(repo => repo.Get(It.IsIn<int>(0, 1)), Times.Exactly(2));
        }
    }
}
