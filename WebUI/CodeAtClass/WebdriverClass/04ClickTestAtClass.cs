using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace WebdriverClass
{
    class ClickTestAtClass : TestBase
    {
        [Test]
        public void ClickExample()
        {
            Driver.Navigate().GoToUrl("https://stackoverflow.com/tags");

            //Click on Questions link (use Id selector)
            // it doesn't so i can't use that :(
            Driver.FindElement(By.CssSelector("a.js-gps-track.-link")).Click();
            Assert.AreEqual("Newest Questions - Stack Overflow", Driver.Title);

            //Click on Tags link (use Id selector)
            Driver.FindElement(By.Id("nav-tags")).Click();
            Assert.AreEqual("Tags - Stack Overflow", Driver.Title);

            //Click on Stackoverflow logo link (use CssSelector selector)
            Driver.FindElement(By.CssSelector("span.-img._glyph"));
            StringAssert.Contains("Stack Overflow", Driver.Title);
        }

        [Test]
        public void ShiftClickExample()
        {
            Driver.Navigate().GoToUrl("https://www.amazon.com");
            //Use Actions class to create a shift click on Cart link
            new Actions(Driver).KeyDown(Keys.Shift).Build().Perform();
            Driver.FindElement(By.CssSelector("span.nav-cart-icon.nav-sprite")).Click();
            //You have to see two browser windows after a successful run
            Assert.AreEqual(2, Driver.WindowHandles.Count);
        }

        [Test]
        public void DragAndDropTestAtClass()
        {
            Driver.Navigate().GoToUrl("https://jqueryui.com/resources/demos/droppable/default.html");
            //Locate the draggable element
            var draggable =  Driver.FindElement(By.Id("draggable"));
            //Locate the droppable element
            var droppable = Driver.FindElement(By.Id("droppable"));
            //Add the drag and drop action here
            new Actions(Driver).DragAndDrop(draggable, droppable).Build().Perform();
            Assert.IsTrue(Driver.FindElement(By.CssSelector("#droppable > p:nth-child(1)")).Text.Equals("Dropped!"));
        }
    }
}
