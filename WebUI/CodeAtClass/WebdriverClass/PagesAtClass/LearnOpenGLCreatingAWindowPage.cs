using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    class LearnOpenGLCreatingAWindowPage: BasePage
    {
        public LearnOpenGLCreatingAWindowPage(IWebDriver webDriver) : base(webDriver)
        {
            new WebDriverWait(Driver, TimeSpan.FromSeconds(20)).Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("button.css-47sehv"))).Click();

        }

        private IWebElement glfwDownloadLink => Driver.FindElement(By.CssSelector("a[href*='http://www.glfw.org/download.html']"));
        private IWebElement githubLink => Driver.FindElement(By.CssSelector("a[href*='https://github.com/JoeyDeVries/LearnOpenGL']"));
        private IWebElement twitterLink => Driver.FindElement(By.CssSelector("a[href*='https://twitter.com/JoeyDeVriez']"));
        
        public static LearnOpenGLCreatingAWindowPage NavigateTo(IWebDriver driver)
        {
            driver.Navigate().GoToUrl("https://learnopengl.com/Getting-started/Creating-a-window");
            return new LearnOpenGLCreatingAWindowPage(driver);
        }

        public GlfwDownloadPage NavigateToGlfwDowloadPage()
        {
            new WebDriverWait(Driver, TimeSpan.FromSeconds(20)).Until(ExpectedConditions.ElementToBeClickable(glfwDownloadLink));
            glfwDownloadLink.Click();
            Driver.SwitchTo().Window(Driver.WindowHandles[Driver.WindowHandles.Count - 1]);
            return new GlfwDownloadPage(Driver);
        }


        public GithubPage NavigateToGithubPage()
        {
            new WebDriverWait(Driver, TimeSpan.FromSeconds(20)).Until(ExpectedConditions.ElementToBeClickable(githubLink));
            githubLink.Click();
            Driver.SwitchTo().Window(Driver.WindowHandles[Driver.WindowHandles.Count - 1]);
            return new GithubPage(Driver);
        }

        public TwitterPage NavigateToTwitterPage()
        {
            new WebDriverWait(Driver, TimeSpan.FromSeconds(20)).Until(ExpectedConditions.ElementToBeClickable(twitterLink));
            twitterLink.Click();
            Driver.SwitchTo().Window(Driver.WindowHandles[Driver.WindowHandles.Count - 1]);
            return new TwitterPage(Driver);
        }
    }   
}
