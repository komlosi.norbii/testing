using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.PagesAtClass
{
    class GithubPage: BasePage
    {
        public GithubPage(IWebDriver webDriver) : base(webDriver)
        {

        }

        public IWebElement GetGoToFileLink()
        {
            return new WebDriverWait(Driver, TimeSpan.FromSeconds(10)).Until(d => d.FindElement(By.CssSelector("a[href*='/JoeyDeVries/LearnOpenGL/find/master']")));
        }
    }
}
