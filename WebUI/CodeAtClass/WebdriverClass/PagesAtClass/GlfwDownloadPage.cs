using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.PagesAtClass
{
    class GlfwDownloadPage: BasePage
    {
        public GlfwDownloadPage(IWebDriver webDriver) : base(webDriver)
        {

        }

        public IWebElement GetGlfwHomeRef()
        {
            return new WebDriverWait(Driver, TimeSpan.FromSeconds(10)).Until(d => d.FindElement(By.Id("glfw_home")));
        }
    }
}
