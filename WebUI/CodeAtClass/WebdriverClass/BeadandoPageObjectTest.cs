using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass
{
    class BeadandoPageObjectTest: TestBase
    {
        [Test]
        public void ClickGlfwDownloadLinkAndHomePageLinkVisible()
        {
            var glfwHomePageLink = LearnOpenGLCreatingAWindowPage.NavigateTo(Driver)
                .NavigateToGlfwDowloadPage()
                .GetGlfwHomeRef();

            Assert.IsTrue(glfwHomePageLink.Displayed);
        }

        [Test]
        public void ClickGithubLinkAndHomePageLinkVisible()
        {
            var goToFileLink = LearnOpenGLCreatingAWindowPage.NavigateTo(Driver)
                .NavigateToGithubPage()
                .GetGoToFileLink();

            Assert.IsTrue(goToFileLink.Displayed);
        }

        [Test]
        public void ClickTwitterLinkAndNotificationsLinkVisible()
        {
            var exploreLink = LearnOpenGLCreatingAWindowPage.NavigateTo(Driver)
                .NavigateToTwitterPage()
                .GetExploreLink();

            Assert.IsTrue(exploreLink.Displayed);
        }
    }
}
