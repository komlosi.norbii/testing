using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System.Collections.ObjectModel;
using System.IO;

namespace WebdriverClass
{
    class ScreenShotsTestAtClass : TestBase
    {
        [Test]
        public void ScreenShots()
        {
            Driver.Navigate().GoToUrl("http://www.elvira.hu");

            string baseDirectory = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "\\screenshot\\"));
            Directory.CreateDirectory(baseDirectory);
            string screenshotPath = baseDirectory + "screenshot.png";
            //Maximize browser window
            Driver.Manage().Window.Maximize();
            //Create a screenshot and save the file as screenshot.png
            ((ITakesScreenshot)Driver).GetScreenshot().SaveAsFile(screenshotPath, ScreenshotImageFormat.Png);
            Assert.IsTrue(File.Exists(screenshotPath));
        }
    }
}
