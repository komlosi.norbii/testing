using NUnit.Framework;
using OpenQA.Selenium;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    class ResultWidget : BasePage
    {
        public ResultWidget(IWebDriver driver) : base(driver)
        {
        }

        // TASK 4.2: Create a new timetable webelement using FindsBy annotation

        public int GetNoOfResults()
        {
            // TASK 4.3: Count the valid "tr" tags inside timetable webelement and return the number
            return 0;
        }
    }
}
